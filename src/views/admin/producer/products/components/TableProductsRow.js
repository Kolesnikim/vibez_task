import {Button, Flex, Text} from "@chakra-ui/react";
import {currency_dict, product_type_dict} from "../../../../../dictionaries";
import React, { memo } from "react";

export const TableProductsRow = memo(function({ item, textColor, toggleClosure }) {
    return (
        <tr key={item.id}>
            <td>
                <Flex align="center" marginInline={"24px"} marginTop="32px">
                    <Text
                        me="10px"
                        color={textColor}
                        fontSize="sm"
                        fontWeight="700"
                    >
                        {item?.name}
                    </Text>
                </Flex>
            </td>
            <td>
                <Flex align="center" marginInline={"24px"} marginTop="32px">
                    <Text
                        me="10px"
                        color={textColor}
                        fontSize="sm"
                        fontWeight="700"
                    >
                        {item?.status}
                    </Text>
                </Flex>
            </td>
            <td>
                <Flex align="center" marginInline={"24px"} marginTop="32px">
                    <Text
                        me="10px"
                        color={textColor}
                        fontSize="sm"
                        fontWeight="700"
                    >
                        {product_type_dict[item?.product_type]}
                    </Text>
                </Flex>
            </td>
            <td>
                <Flex align="center" marginInline={"24px"} marginTop="32px">
                    <Text
                        me="10px"
                        color={textColor}
                        fontSize="sm"
                        fontWeight="700"
                    >
                        {item?.description}
                    </Text>
                </Flex>
            </td>
            <td>
                <Flex align="center" marginInline={"24px"} marginTop="32px">
                    <Text
                        me="10px"
                        color={textColor}
                        fontSize="sm"
                        fontWeight="700"
                    >
                        {currency_dict["ILS"]}
                        {item?.price}
                    </Text>
                </Flex>
            </td>
            <td>
                <Flex align="center" marginInline={"24px"} marginTop="32px">
                    <Text
                        me="10px"
                        color={textColor}
                        fontSize="sm"
                        fontWeight="700"
                    >
                        {item?.stock}
                    </Text>
                </Flex>
            </td>
            <td>
                <Flex align="center" marginInline={"24px"} marginTop="32px">
                    <Text
                        me="10px"
                        color={textColor}
                        fontSize="sm"
                        fontWeight="700"
                    >
                        {item?.reserved}
                    </Text>
                </Flex>
            </td>
            <td>
                <Flex align="center" marginInline={"24px"} marginTop="32px">
                    <Text
                        me="10px"
                        color={textColor}
                        fontSize="sm"
                        fontWeight="700"
                    >
                        {item?.sold}
                    </Text>
                </Flex>
            </td>

            <td>
                <Flex align="center" marginInline={"24px"} marginTop="32px">
                    <Button
                        onClick={() => toggleClosure(item)}
                        variant={"brand"}
                        minW={"100px"}
                        size="md"
                        fontWeight="500"
                        borderRadius="70px"
                        mx="10px"
                    >
                        Edit
                    </Button>
                </Flex>
            </td>
        </tr>
    )
}
)